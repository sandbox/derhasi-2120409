<?php

/**
 * @file
 * Holds token hooks for tokenpane.
 */

/**
 * Implements hook_token_info().
 */
function tokenpane_token_info() {
  $tokens = array();

  // Each tokenpane key is a token in the tokenpane type.
  $tokenpane_keys = tokenpanes_get_keys();
  foreach ($tokenpane_keys as $key => $dids) {
    $tokens[$key] = array(
      'name' => t('TokenPane @key', array('@key' => $key)),
      'description' => t('The string value from the tokenpane key %key', array('%key' => $key)),
    );
  }

  return array(
    'types' => array(
      'tokenpane' => array(
        'name' => t('TokenPane'),
        'description' => t('Token replacements defined in a pane.'),
      ),
    ),
    'tokens' => array('tokenpane' => $tokens),
  );

}

/**
 * Implements hook_tokens().
 */
function tokenpane_tokens($type, $tokens, array $data = array(), array $options = array()) {

  $return = array();
  if ($type == 'tokenpane') {
    foreach ($tokens as $token => $pattern) {
      $return[$pattern] = tokenpane_get_value($token);
    }
  }
  return $return;
}
