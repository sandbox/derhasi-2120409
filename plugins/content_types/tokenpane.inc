<?php

/**
 * @file
 * A pane that is for simply providing tokens for other panes.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('TokenPane'),
  'description' => t('Pane to build tokens for global usage.'),

  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('tokenpane'),
  // Name of a function which will render the block.
  'render callback' => 'tokenpane_content_type_tokenpane_render',
  // The default context.
  'defaults' => array(),
  'all contexts' => TRUE,

  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_no_context_content_type_edit_form.
  'edit form' => 'tokenpane_content_type_tokenpane_edit_form',

  // Icon goes in the directory with the content type.
  'icon' => 'icon_example.png',
  'category' => array(t('Miscellaneous'), 10),


);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function tokenpane_content_type_tokenpane_render($subtype, $conf, $args, $context) {
  $pane = new stdClass();
  $pane->title = '';
  $conf = tokenpane_content_type_tokenpane_keyword_substitute($conf, $context);

  foreach ($conf['tokens'] as $token) {
    if (drupal_strlen($token['key']) > 0) {
      tokenpane_push_value($token['key'], $token['value']);
    }
  }

  tokenpanes_get_keys();

  // Nothing to render.
  return array();
}

/**
 * 'Edit form' callback for the content type.
 */
function tokenpane_content_type_tokenpane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  if (empty($conf['tokens'])) {
    $conf['tokens'] = array();
  }
  // Provide some new items.
  $conf['tokens'][] = array('key' => '', 'value' => '');
  $conf['tokens'][] = array('key' => '', 'value' => '');
  $conf['tokens'][] = array('key' => '', 'value' => '');


  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tokens'),
    '#tree' => TRUE,
  );

  $i = 0;
  foreach ($conf['tokens'] as $token) {
    $i++;
    $form['tokens'][] = array(
      'key' => array(
        '#type' => 'textfield',
        '#title' => t('key #@nr', array('@nr' => $i)),
        '#default_value' => $token['key'],
        '#required' => FALSE,
      ),
      'value' => array(
        '#type' => 'textfield',
        '#title' => t('value #@nr', array('@nr' => $i)),
        '#default_value' => $token['value'],
      ),
    );
  }

  return $form;
}


function tokenpane_content_type_tokenpane_edit_form_submit($form, &$form_state) {

  // Save token association.
  $tokens = $form_state['values']['tokens'];
  $form_state['conf']['tokens'] = array();
  foreach ($tokens as $token) {
    if (drupal_strlen($token['key']) > 0) {
      $form_state['conf']['tokens'][] = $token;
    }
  }

}

function tokenpane_content_type_tokenpane_keyword_substitute($conf, $context) {

  foreach ($conf['tokens'] as $key => $token) {
    $conf['tokens'][$key]['value'] = ctools_context_keyword_substitute($token['value'], array(), $context);
  }

  return $conf;
}
